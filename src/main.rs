// use std::env;
use gtk::prelude::*;
use gtk::{/*glib,*/ Application, ApplicationWindow, Button}; 

const APP_ID: &str = "org.gtk_rs.rust-gtk-app1";

enum ReturnValue {
    Success = 0x0000,
    Failed = 0x00ff
}

fn main() 
{
    std::process::exit(main2());

}

fn main2() -> i32 
{ 
     println!("Hello, world!");
    
    // Create a new application
    let app = Application::builder().application_id(APP_ID).build();

    // Connect to "activate" signal of `app`
    app.connect_activate(build_ui); 

    // Run the application
    app.run();

    if true //TODO: handle Results here
    {
        return ReturnValue::Success as i32;
    }
    else 
    {
        return ReturnValue::Failed as i32;
    }
}

fn build_ui(app: &Application) { 
    // Create a button with label and margins
    let button = Button::builder()
        .label("Press me!")
        .margin_top(12)
        .margin_bottom(12)
        .margin_start(12)
        .margin_end(12)
        .build();

    // Connect to "clicked" signal of `button`
    button.connect_clicked(|button| {
        // Set the label after the button has been clicked on
        button.set_label("Press me again.");
    });

    // Create a window and set the title
    let window = ApplicationWindow::builder()
        .application(app)
        .title("rust-gtk-app1")
        .child(&button)
        .build();

    // Present window
    window.present();
}
