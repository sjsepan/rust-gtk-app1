# Rust GTK App 1 (rust-gtk-app1)

## About

Rust app using GTK 4. Steps performed on Linux Mint 21.3 using VSCodium 1.85.2 and rust-analyzer 0.4.1826 (pre-release).
This was my experience following the directions on <https://gtk-rs.org/gtk4-rs>

## Setup

Prepare Linux.
Install 'rustup' with instructions at <https://rustup.rs/>

Add essential libraries (Debian flavor of instructions):

`sudo apt install libgtk-4-dev build-essential`

Create the project:

`cargo new rust-gtk-app1`

`cd rust-gtk-app1`

Check GTK version:

`pkg-config --modversion gtk4`

The output version will be in the format 'major.minor.rev':

    4.6.9

Provide that in the next command in the format 'major_minor'.

`cargo add gtk4 --rename gtk --features v4_6`

The output may look something like:

    Updating crates.io index
      Adding gtk4 v0.7.3 to dependencies.
             Features:
             + v4_2
             + v4_4
             + v4_6
             - blueprint
             - gnome_42
             - gnome_43
             - gnome_44
             - gnome_45
             - unsafe-assume-initialized
             - v4_10
             - v4_12
             - v4_14
             - v4_8
             - xml_validation

Test the template application:

`cargo run`
or
`cargo run --package rust-gtk-app1 --bin rust-gtk-app1`

The output may look something like:

    Compiling serde v1.0.196
    Compiling target-lexicon v0.12.13
    Compiling proc-macro2 v1.0.78
    Compiling libc v0.2.153
    Compiling cfg-expr v0.15.6
    Compiling quote v1.0.35
    Compiling syn v1.0.109
    Compiling proc-macro-error-attr v1.0.4
    Compiling slab v0.4.9
    Compiling syn v2.0.48
    Compiling proc-macro-error v1.0.4
    Compiling thiserror v1.0.56
    Compiling gio v0.18.4
    Compiling semver v1.0.21
    Compiling futures-io v0.3.30
    Compiling rustc_version v0.4.0
    Compiling memoffset v0.9.0
    Compiling anyhow v1.0.79
    Compiling toml_datetime v0.6.3
    Compiling serde_spanned v0.6.5
    Compiling toml_edit v0.20.2
    Compiling toml_edit v0.19.15
    Compiling toml v0.8.2
    Compiling futures-macro v0.3.30
    Compiling system-deps v6.2.0
    Compiling thiserror-impl v1.0.56
    Compiling proc-macro-crate v2.0.2
    Compiling glib-sys v0.18.1
    Compiling gobject-sys v0.18.0
    Compiling gio-sys v0.18.1
    Compiling futures-util v0.3.30
    Compiling glib-macros v0.18.5
    Compiling gdk-pixbuf-sys v0.18.0
    Compiling pango-sys v0.18.0
    Compiling futures-executor v0.3.30
    Compiling cairo-sys-rs v0.18.2
    Compiling gdk4-sys v0.7.2
    Compiling graphene-sys v0.18.1
    Compiling gsk4-sys v0.7.3
    Compiling gtk4-sys v0.7.3
    Compiling field-offset v0.3.6
    Compiling proc-macro-crate v1.3.1
    Compiling gtk4-macros v0.7.2
    Compiling glib v0.18.5
    Compiling cairo-rs v0.18.5
    Compiling graphene-rs v0.18.1
    Compiling gdk-pixbuf v0.18.5
    Compiling pango v0.18.3
    Compiling gdk4 v0.7.3
    Compiling gsk4 v0.7.3
    Compiling gtk4 v0.7.3
    Compiling rust-gtk-app1 v0.1.0 (/home/sjsepan/Projects/Rust/rust-gtk-app1)
     Finished dev [unoptimized + debuginfo] target(s) in 59.42s
      Running 'target/debug/rust-gtk-app 1'
    Hello, world!

## Hello World

Using the example at this page (<https://gtk-rs.org/gtk4-rs/stable/latest/book/hello_world.html>) start by replacing the Hello World template with:

    use gtk::prelude::*;
    use gtk::{glib, Application};

    const APP_ID: &str = "org.gtk_rs.rust-gtk-app1";

    fn main() -> glib::ExitCode {
        println!("Hello, world!");
        
        // Create a new application
        let app = Application::builder().application_id(APP_ID).build();

        // Run the application
        app.run()
    }

Your output may look like:

    Compiling glib-sys v0.18.1
    Compiling field-offset v0.3.6
    Compiling gobject-sys v0.18.0
    Compiling cairo-sys-rs v0.18.2
    Compiling graphene-sys v0.18.1
    Compiling gio-sys v0.18.1
    Compiling pango-sys v0.18.0
    Compiling glib v0.18.5
    Compiling gdk-pixbuf-sys v0.18.0
    Compiling gdk4-sys v0.7.2
    Compiling gsk4-sys v0.7.3
    Compiling gtk4-sys v0.7.3
    Compiling gio v0.18.4
    Compiling cairo-rs v0.18.5
    Compiling graphene-rs v0.18.1
    Compiling gdk-pixbuf v0.18.5
    Compiling pango v0.18.3
    Compiling gdk4 v0.7.3
    Compiling gsk4 v0.7.3
    Compiling gtk4 v0.7.3
    Compiling rust-gtk-app1 v0.1.0 (/home/sjsepan/Projects/Rust/rust-gtk-app1)
        Finished dev [unoptimized + debuginfo] target(s) in 54.92s
        Running `target/debug/rust-gtk-app1`
    Hello, world!

    (rust-gtk-app1:47202): GLib-GIO-WARNING **: 11:35:23.867: Your application does not implement g_application_activate() and has no handlers connected to the 'activate' signal.  It should do one of these.

Note that no window appears, and except for the 'println' output (left in as a test), there is only a warning.
Now update the code to look like this:

    use gtk::prelude::*;
    use gtk::{glib, Application, ApplicationWindow}; //ADD THIS...

    const APP_ID: &str = "org.gtk_rs.rust-gtk-app1";

    fn main() -> glib::ExitCode {
        println!("Hello, world!");
        
        // Create a new application
        let app = Application::builder().application_id(APP_ID).build();

        // Connect to "activate" signal of `app`
        app.connect_activate(build_ui); //...AND THIS...

        // Run the application
        app.run()
    }

    fn build_ui(app: &Application) { //...AND THIS
        // Create a window and set the title
        let window = ApplicationWindow::builder()
            .application(app)
            .title("rust-gtk-app1")
            .build();

        // Present window
        window.present();
    }

Run again as before; the output may be like...

    Compiling rust-gtk-app1 v0.1.0 (/home/sjsepan/Projects/Rust/rust-gtk-app1)
        Finished dev [unoptimized + debuginfo] target(s) in 5.14s
        Running `target/debug/rust-gtk-app1`
    Hello, world!

.. and ...

![rust-gtk-app1_1.png](./rust-gtk-app1_1.png?raw=true "Screenshot")

## TODO

Create an example form for the app...

## Contact

Stephen J Sepan

<sjsepan@yahoo.com>

2-1-2024

---
